// Karma configuration
// Generated on Sun Apr 21 2024 02:01:45 GMT+0200 (Central European Summer Time)
process.env.CHROME_BIN = require("puppeteer").executablePath();

module.exports = function (config) {
  config.set({
    // start these browsers
    // available browser launchers: https://www.npmjs.com/search?q=keywords:karma-launcher
    browsers: ["ChromeHeadlessCI"],
    customLaunchers: {
      ChromeHeadlessCI: {
        base: "ChromiumHeadless",
        arguments: ["--no-sandbox"],
        flags: ["--no-sandbox", "--disable-gpu"],
      },
    },

    frameworks: ["jasmine", '@angular-devkit/build-angular'],

    preprocessors: {
      'WebSDK.js': ['coverage'],
    },

    plugins: [
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require('karma-jasmine-html-reporter'),
      require('karma-spec-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],

    angularCli: {
      config: './angular.json',
      environment: 'dev'
    },

    client:{
      clearContext: false
    },

    mime: {
      'text/x-typescript': ['ts','tsx']
    },

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: "",

    // list of files / patterns to exclude
    exclude: [],

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
    reporters: ['spec', 'kjhtml', 'progress', 'coverage'],

    coverageReporter: {
      type: 'html',
      dir: 'coverage/',
      reporters: [
        { type: 'html' },
        { type: 'text-summary' },
        { type: 'lcovonly' },
        { type: 'cobertura' },
      ],
      check: {
        global: {
          statements: 10,
          branches: 10,
          functions: 10,
          lines: 10,
        },
      },
    },
    specReporter: {
      maxLogLines: 5,
      suppressErrorSummary: true,
      suppressFailed: false,
      suppressPassed: false,
      suppressSkipped: true,
      showSpecTiming: false
    },

    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: true,
    browserNoActivityTimeout: 1000 * 60,
    concurrency: Infinity,
  });
};
