import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImageComponent } from './image.component';

describe('ImageComponent', () => {
  let component: ImageComponent;
  let fixture: ComponentFixture<ImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the src attribute of the image to the placeholder image on error', () => {
    const imgElement = fixture.nativeElement.querySelector('img');
    const event = new Event('error');
    imgElement.dispatchEvent(event);
    expect(imgElement.src).toContain('placeholder.png');
  });

  it('should set the src attribute of the image to the provided src', () => {
    component.src = 'test.jpg';
    fixture.detectChanges();
    const imgElement = fixture.nativeElement.querySelector('img');
    expect(imgElement.src).toContain('test.jpg');
  });



});
