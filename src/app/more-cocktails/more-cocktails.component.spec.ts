import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MoreCocktailsComponent } from './more-cocktails.component';
import { CocktailService } from '@app/services/cocktail.service';
import { of } from 'rxjs';
import { Cocktail } from '@app/models/cocktail.model';
import { ImageComponent } from '@app/image/image.component';

describe('MoreCocktailsComponent', () => {
  let component: MoreCocktailsComponent;
  let fixture: ComponentFixture<MoreCocktailsComponent>;
  let mockCocktailService: jasmine.SpyObj<CocktailService>;

  const mockCocktail: Cocktail = {
    id: '1',
    name: 'Mojito',
    image: 'mojito.jpg',
    category: 'Cocktail',
    alcoholic: 'Alcoholic',
    glass: 'Highball glass',
    instructions: 'Mix and serve over ice',
    ingredients: ['Rum', 'Lime', 'Mint', 'Sugar', 'Soda water'],
    measures: ['2 oz', '1 oz', '6 leaves', '2 tsp', 'Top'],
  };

  beforeEach(async () => {
    mockCocktailService = jasmine.createSpyObj('CocktailService', ['getRandom']);

    // Configure the spy to return the same cocktail each time getRandom() is called
    mockCocktailService.getRandom.and.returnValue(of([mockCocktail]));

    await TestBed.configureTestingModule({
      declarations: [
        MoreCocktailsComponent,
        ImageComponent // Declare the app-image component here
      ],
      providers: [
        { provide: CocktailService, useValue: mockCocktailService }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreCocktailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set isLoading to false when all cocktails are loaded', () => {
    expect(component.isLoading).toBeFalse();
  });

  it('should load the same cocktail three times', () => {
    expect(component.cocktails.length).toBe(3);
    expect(component.cocktails.every(cocktail => cocktail.name === 'Mojito')).toBeTrue();
    expect(component.cocktails.every(cocktail => cocktail.image === './assets/img/placeholder.png' || cocktail.image === 'mojito.jpg')).toBeTrue();
  });

});
