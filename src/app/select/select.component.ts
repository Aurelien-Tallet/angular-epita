import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() options: Array<any> = [];
  selected = '';
  @Output() selectEvent = new EventEmitter<string>();
  constructor(private route: ActivatedRoute, private router: Router) {
  }
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['category']) {
        this.selected = params['category'];
      }
    });
  }

  select(event: unknown) {
    this.selectEvent.emit(event as string);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { category: event, page: 1 },
      queryParamsHandling: 'merge'
    });
  }
}
