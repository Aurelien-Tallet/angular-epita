import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { CocktailService } from '@app/services/cocktail.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {of} from 'rxjs';
import { Cocktail } from '@app/models/cocktail.model';

import { InputSearchComponent } from "@app/components/input-search/input-search.component";
import { SelectComponent } from "@app/select/select.component";
import { ListItemComponent } from "@app/components/list-item/list-item.component";
import { ImageComponent } from "@app/image/image.component";
import { nonAlcoholicPipe } from "@app/pipes/non-alcoholic.pipe";
import { categoryPipe } from "@app/pipes/category.pipe";

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockCocktailService: jasmine.SpyObj<CocktailService>;

  const mockCocktails: Cocktail[] = [
    {
      id: '1',
      name: 'Mojito',
      image: 'mojito.jpg',
      category: 'Cocktail',
      alcoholic: 'Alcoholic',
      glass: 'Highball glass',
      instructions: 'Mix and serve over ice',
      ingredients: ['Rum', 'Lime', 'Mint', 'Sugar', 'Soda water'],
      measures: ['2 oz', '1 oz', '6 leaves', '2 tsp', 'Top'],
    },
    {
      id: '2',
      name: 'Cosmopolitan',
      image: 'cosmopolitan.jpg',
      category: 'Cocktail',
      alcoholic: 'Alcoholic',
      glass: 'Martini Glass',
      instructions: 'Shake and strain into a chilled cocktail glass',
      ingredients: ['Vodka', 'Triple sec', 'Lime juice', 'Cranberry juice'],
      measures: ['1.5 oz', '1 oz', '0.5 oz', '1 oz'],
    }
  ];

  beforeEach(async () => {
    mockCocktailService = jasmine.createSpyObj('CocktailService', ['searchByName', 'getCategories']);

    mockCocktailService.searchByName.and.returnValue(of(mockCocktails));
    mockCocktailService.getCategories.and.returnValue(of([
      'Non-alcoholic', 'Cocktail', 'Beer'
    ]));

    await TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        InputSearchComponent,
        SelectComponent,
        ListItemComponent,
        ImageComponent,
        nonAlcoholicPipe,
        categoryPipe
      ],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        { provide: CocktailService, useValue: mockCocktailService },
        { provide: ActivatedRoute, useValue: {
            queryParams: of({ search: 'test', page: 1 })
        }},
        { provide: Router, useValue: {} }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch cocktails on ngOnInit', () => {
    mockCocktailService.searchByName.and.returnValue(of(mockCocktails));

    component.ngOnInit();

    expect(component.cocktails).toEqual(mockCocktails);
    expect(component.isLoading).toBe(false);
  });

  it('should set category on category selection', () => {
    const category = 'Non-alcoholic';
    component.onSelectCategory(category);

    expect(component.category).toBe(category);
  });

  it('should fetch cocktails on search', () => {
    const searchText = 'Margarita';
    const mockCocktails: Cocktail[] = [{ id: '1', name: 'Margarita', ingredients: [], measures: [] }];
    mockCocktailService.searchByName.and.returnValue(of(mockCocktails));

    component.onSearch(searchText);

    expect(component.cocktails).toEqual(mockCocktails);
    expect(component.isLoading).toBe(false);
  });
});
