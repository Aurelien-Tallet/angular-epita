import { Component, OnInit } from '@angular/core';
import { CocktailService } from "@app/services/cocktail.service";
import { FormGroup, FormControl } from '@angular/forms';
import { Cocktail } from '@app/models/cocktail.model';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  cocktails: Array<Cocktail> = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  categories: Array<any> = [];
  isLoading = true;
  form: FormGroup;
  constructor(private cocktailService: CocktailService, private route: ActivatedRoute, private router: Router) {
    this.form = new FormGroup({
      nonAlcoholic: new FormControl(false),
      category: new FormControl(""),
    });
  }
  ngOnInit(): void {
    this.cocktailService.searchByName("").subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
      this.isLoading = false;
    });
    this.cocktailService.getCategories().subscribe((categories) => {
      this.categories = categories;

    });
  }

  get nonAlcoholic(): boolean { return this.form.get("nonAlcoholic")?.value; }
  get category(): string { return this.form.get("category")?.value; }
  onSearch(searchText: string) {
    this.isLoading = true;
    this.cocktailService.searchByName(searchText).subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
      this.isLoading = false;
    });
  }
  onSelectCategory(category: string) {
    this.form.get("category")?.setValue(category);
  }
}
