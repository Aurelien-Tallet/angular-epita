export interface Category { strCategory: string; }
export interface CategoriesResponse { drinks: Category[]; }