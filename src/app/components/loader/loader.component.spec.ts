import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoaderComponent } from './loader.component';

describe('LoaderComponent', () => {
  let component: LoaderComponent;
  let fixture: ComponentFixture<LoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have loading set to false by default', () => {
    expect(component.loading).toBeFalse();
  });

  it('should display loader when loading is true', () => {
    component.loading = true;
    fixture.detectChanges();
    const loaderElement = fixture.nativeElement.querySelector('.loader');
    expect(loaderElement).toBeTruthy();
  });

  it('should not display loader when loading is false', () => {
    component.loading = false;
    fixture.detectChanges();
    const loaderElement = fixture.nativeElement.querySelector('.mock-loader');
    expect(loaderElement).toBeFalsy();
  });
});
