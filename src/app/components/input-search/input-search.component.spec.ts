import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputSearchComponent } from './input-search.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('InputSearchComponent', () => {
  let component: InputSearchComponent;
  let fixture: ComponentFixture<InputSearchComponent>;

 
  const mockActivatedRoute = {
    queryParams: of({ search: 'test', page: 1 })
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSearchComponent ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
