import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';



type Timeout = ReturnType<typeof setTimeout> | null;


@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent implements OnInit {

  searchText = '';

  @Output() searchEvent = new EventEmitter<string>();

  timeOut: Timeout = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['search'] || params['page']) {
        const text = params['search'] !== undefined ? params['search'] : '';
        this.searchText = text;
        this.searchEvent.emit(text);
      }
    });
  }


  search(event: KeyboardEvent) {
    if (this.timeOut) {
      clearTimeout(this.timeOut);
    }
    this.timeOut = setTimeout(() => {
      this.searchEvent.emit((event.target as HTMLInputElement).value);
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { search: (event.target as HTMLInputElement).value, page: 1 },
        queryParamsHandling: 'merge'
      });
    }, 200);
  }
}
