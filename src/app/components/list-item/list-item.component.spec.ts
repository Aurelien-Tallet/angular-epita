import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListItemComponent } from './list-item.component';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

// Définition du mock pour app-skeleton
@Component({
  selector: 'app-skeleton',
  template: '<div class="mock-skeleton">Mock Skeleton</div>'
})
class MockSkeletonComponent {}

describe('ListItemComponent', () => {
  let component: ListItemComponent;
  let fixture: ComponentFixture<ListItemComponent>;

  const mockActivatedRoute = {
    queryParams: of({})
  };

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        ListItemComponent,
        MockSkeletonComponent // Utilisation du mock ici
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: Router, useValue: mockRouter }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize currentPage to 1 if no page query param', () => {
    expect(component.currentPage).toBe(1);
  });
});
