import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent implements OnInit {
  skeletons: Array<number> = [];
  @Input() number = 9;

  ngOnInit(): void {
    this.skeletons = [...Array(this.number).keys()];
  }
}
