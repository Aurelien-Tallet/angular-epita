import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CocktailService } from './cocktail.service';

describe('CocktailService', () => {
  let service: CocktailService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CocktailService]
    });
    service = TestBed.inject(CocktailService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable<Cocktail[]>', () => {
    service.searchByName('Margarita').subscribe(cocktails => {
      expect(cocktails.length).toBe(1);
    });

    const req = httpTestingController.expectOne('https://the-cocktail-db.p.rapidapi.com/search.php?s=Margarita');

    req.flush({
      drinks: [
        {
          idDrink: '1',
          strDrink: 'Margarita',
          strDrinkThumb: 'margarita.jpg',
          strCategory: 'Cocktail',
          strAlcoholic: 'Alcoholic',
          strGlass: 'Cocktail glass',
          strInstructions: 'Shake and strain',
          strIngredient1: 'Tequila',
          strMeasure1: '2 oz',
          strIngredient2: 'Triple sec',
          strMeasure2: '1 oz',
          strIngredient3: 'Lime juice',
          strMeasure3: '1 oz',
          strIngredient4: 'Salt',
          strMeasure4: '1 pinch',
        }
      ]
    });

    httpTestingController.verify();
  });

  it('should return an Observable<Cocktail[]> with calling getSingle', () => {
    service.getSingle('1').subscribe(cocktails => {
      expect(cocktails.length).toBe(1);
    });

    const req = httpTestingController.expectOne('https://the-cocktail-db.p.rapidapi.com/lookup.php?i=1');

    req.flush({
      drinks: [
        {
          idDrink: '1',
          strDrink: 'Margarita',
          strDrinkThumb: 'margarita.jpg',
          strCategory: 'Cocktail',
          strAlcoholic: 'Alcoholic',
          strGlass: 'Cocktail glass',
          strInstructions: 'Shake and strain',
          strIngredient1: 'Tequila',
          strMeasure1: '2 oz',
          strIngredient2: 'Triple sec',
          strMeasure2: '1 oz',
          strIngredient3: 'Lime juice',
          strMeasure3: '1 oz',
          strIngredient4: 'Salt',
          strMeasure4: '1 pinch',

  }
      ]
    });

    httpTestingController.verify();
  }
  );

  it('should return an Observable<Cocktail[]> with calling getRandom', () => {
    service.getRandom().subscribe(cocktails =>
      expect(cocktails.length).toBe(1)
    );
  const req = httpTestingController.expectOne('https://the-cocktail-db.p.rapidapi.com/random.php');

  req.flush({
    drinks: [
      {
        idDrink: '1',
        strDrink: 'Margarita',
        strDrinkThumb: 'margarita.jpg',
        strCategory: 'Cocktail',
        strAlcoholic: 'Alcoholic',
        strGlass: 'Cocktail glass',
        strInstructions: 'Shake and strain',
        strIngredient1: 'Tequila',
        strMeasure1: '2 oz',
        strIngredient2: 'Triple sec',
        strMeasure2: '1 oz',
        strIngredient3: 'Lime juice',
        strMeasure3: '1 oz',
        strIngredient4: 'Salt',
        strMeasure4: '1 pinch',
      }
    ]
  });



}
  );

  it('should return an Observable<string[]> with calling getCategories', () => {
    service.getCategories().subscribe(categories => {
      expect(categories.length).toBe(3);
    });

    const req = httpTestingController.expectOne('http://api.aurelien-nezzar.com/api/angular/getCategories');

    req.flush({
      drinks: [
        { strCategory: 'Non-alcoholic' },
        { strCategory: 'Cocktail' },
        { strCategory: 'Beer' }
      ]
    });

    httpTestingController.verify();
  });
});
